/* If eslint runs in this repository, we get job failed because eslint didn't find any `.js` files.
 *
 * Message:
 * Oops! Something went wrong! :(
 *
 * ESLint: 6.6.0.
 *
 * No files matching the pattern "." were found.
 * Please check for typing mistakes in the pattern.
 */
