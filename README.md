# code-quality

This project contains gitlab-ci templates and docker images that can be reused within multiple projects.
This is helpful for e.g. python code formatting and static analyzers and ensures fast and simple pipelines.

##### howto include pipelines files to your project

* ensure that your project has following stages in this order:
  * `pre_build`
  * `build` 
  * `test` 
* just add to your ci pipelines as shown in this project

**python**

    # needs stages: pre_build , test
    include:
      - project: shared/pipelines/code-quality
        ref: master
        file: python.yml

    # by default jobs use the image `$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA` from the repo including it
    # if your image needs a prepared environment you may overwrite the image for some jobs like prospector as example:
    qa:python:prospector:
      image:
        name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
        entrypoint: ['']
      before_script:
        - pip install -r requirements.txt

**yaml**

    # needs stages: pre_build
    include:
      - project: shared/pipelines/code-quality
        ref: master
        file: yaml.yml
        
**javascript**

    # needs stages: pre_build
    include:
      - project: shared/pipelines/code-quality
        ref: master
        file: javascript.yml
